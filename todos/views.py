from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from todos.models import TodoItem, TodoList


class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"

class ListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"

class ItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/item.html"

class ItemEditUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/edit.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("list_detail")

class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("list_detail")

class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/update.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("list_detail")

class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_lists")

