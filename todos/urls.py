from django.urls import path

from todos.views import (
    TodoListView,
    ListDetailView,
    ItemCreateView,
    ItemEditUpdateView,
    TodoListCreateView,
    TodoListUpdateView,
    TodoListDeleteView,
)

urlpatterns = [
    path("", TodoListView.as_view(), name="todo_lists"),
    path("int:pk/", ListDetailView.as_view(), name="list_detail"),
    path("new/", ItemCreateView.as_view(), name="item_create"),
    path("int:pk/edit/", ItemEditUpdateView.as_view(), name="item_edit"),
    path("new/", TodoListCreateView.as_view(), name="list_create"),
    path("int:pk/edit/", TodoListUpdateView.as_view(), name="list_edit"),
    path("int:pk/delete/", TodoListDeleteView.as_view(), name="list_delete"),
]